//
//  ViewController.m
//  140223_CultureEventPractice
//
//  Created by ctwsine on 2/23/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import "ViewController.h"
#import "detailViewController.h"

@interface ViewController () <NSURLConnectionDataDelegate, UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray* eventList;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    
    //The data below are for dev purposes only, and should be removed after 3rd party data issues being resolved.
    _eventList = @[
    @{@"version": @"1.4",
        @"UID": @"526aa47ee44da0ea8d4168c2",
        @"title": @"2014TIFA－埃梅劇團《愛情剖面》",
        @"category": @"2",
        @"imgURL": @"lesecorches.jpg",
      @"showInfo": @[
    @{
    @"time": @"2014/02/28 19:30:00",
    @"location": @"台北市中山南路21-1號",
    @"locationName": @"國家戲劇院實驗劇場",
    @"onSales": @"Y",
    @"price": @"600",
    @"latitude": @"25.0348366",
    @"longitude": @"121.5176314"
    },
    @{
    @"time": @"2014/03/01 14:30:00",
    @"location": @"台北市中山南路21-1號",
    @"locationName": @"國家戲劇院實驗劇場",
    @"onSales": @"Y",
    @"price": @"600",
    @"latitude": @"25.0348366",
    @"longitude": @"121.5176314"
    }],
        @"masterUnit":@"國立中正文化中心",
        @"webSales": @"http://www.artsticket.com.tw/CKSCC2005/Product/Product00/ProductsDetailsPage.aspx?ProductID=oK4bYlG1Gfxjv%2bgkdzlI5w",
        @"sourceWebPromote": @"http://tifa.ntch.edu.tw",
        @"editModifyDate": @"2014/01/07 14:35:20",
      @"description":@"《愛情剖面》 由法國馬里伏的《爭執》與英國莎拉肯的《渴求》兩個劇本組合而成，兩者皆是揭露殘酷本質的典範之作。前者是寓言，劇情從多個不知世事的青年男女初次相遇開始；後者猶如多聲部的清唱劇，多個沒有名字的角色，他們的過去既共同擁有又互相對峙，戀愛關係中的奇形怪狀，詮釋出「幸福的愛」並不存在，但卻渴求擁有的慾望。\n同一個舞台空間，兩個劇本中的多個角色由同一組演員扮演，導演藉由如此的安排來剖析愛情裡每個面向之間錯綜複雜的情慾質地。從傷痛到傷痛，人類的歷史就是眼淚的歷史，《渴求》將是《爭執》的必然結局。觀眾就如同劇中窺視的成人角色，在冰冷、粗暴、蠻橫、抑鬱的情境中，一個接一個，前仆後繼地，既疏離又沉浸其中。",
        @"sourceWebName": @"兩廳院售票網",
        @"startDate": @"2014/02/28",
        @"endDate": @"2014/03/02",
        @"status": @"success",
        @"total": @"55"
      },
  @{
        @"version": @"1.4",
        @"UID": @"526aa477e44da0ea8d4168c0",
        @"title": @"2014TIFA─與你同行劇團《罪．愛》",
        @"category": @"2",
        @"description":@"《罪‧愛》 的舞台上，艷紅如火的房間裡，一對亂倫相愛的兄妹點燃無限殺機。\n喬凡尼與妹妹安娜貝拉相愛，但他們的父親卻積極為安娜貝拉尋覓良緣，看上青年索藍佐。當兩人婚事的消息傳出後，卻引起索藍佐情人的報復之火；而同時，懷有身孕的安娜貝拉非常懊悔與哥哥的不倫之戀，想要收回對哥哥的誓言，讓一切回到正軌，卻還是事與願違。誰會死在房間裡？是撒旦的遊戲，還是上帝的懲罰？死亡在黑暗的盡頭等待，雙屍謀殺案在頭版上刊。\n奧利佛金獎名導迪克蘭‧唐納倫與設計尼克‧奧姆羅德將故事放至現代，帶入時下年輕人的生活元素，賦予約翰‧福特十七世紀寫就的大膽劇本全新生命。唐納倫精準明快的戲劇節奏，為這部極富爭議性的作品，加入性感、時尚的風貌，看來痛快至極！",
        @"imgURL": @"guiltLove.jpg",
        @"showInfo": @[
  @{
      @"time": @"2014/02/28 19:30:00",
      @"location": @"台北市中山南路21-1號",
      @"locationName": @"國家戲劇院",
      @"onSales": @"Y",
      @"price": @"500,800,1200,1600,2000,2500,3000",
      @"latitude": @"25.0348366",
      @"longitude": @"121.5176314"
      },
  @{
      @"time": @"2014/03/01 19:30:00",
      @"location": @"台北市中山南路21-1號",
      @"locationName": @"國家戲劇院",
      @"onSales": @"Y",
      @"price": @"500,800,1200,1600,2000,2500,3000",
      @"latitude": @"25.0348366",
      @"longitude": @"121.5176314"
      }
  ],
        @"masterUnit": @[
                @"國立中正文化中心"
                ],
        @"webSales": @"http://www.artsticket.com.tw/CKSCC2005/Product/Product00/ProductsDetailsPage.aspx?ProductID=oK4bYlG1GfwhSBj1kuy%2bv",
        @"sourceWebPromote": @"http://tifa.ntch.edu.tw/",
        @"editModifyDate": @"2014/01/13 11:56:46",
        @"sourceWebName": @"兩廳院售票網",
        @"startDate": @"2014/02/28",
        @"endDate": @"2014/03/02",
        @"status": @"success",
        @"total": @"55"
        }];
        //END OF DEV DATA
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - basic ui
- (IBAction)fetchData:(id)sender {
    NSLog(@"This function is currently not in use");
    //[self loadData];
}
- (IBAction)resetData:(id)sender {
    self.eventList = nil;
}

#pragma mark - data processing
- (void) loadData
{
    NSURL* url = [NSURL URLWithString:@"http://cloud.culture.tw/frontsite/trans/SearchShowAction.do?method=doFindTypeJ&category=2&keyword=2014"];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    NSURLConnection* conn = [NSURLConnection connectionWithRequest:request delegate:self];
    [conn start];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"Response received");
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    //check if data exists
    if (data) {
        NSLog(@"%@",data);
    }
    //END OF TRACKING
    NSError *bugReport;
    self.eventList = [NSJSONSerialization JSONObjectWithData:data options:0 error:&bugReport];
    NSLog(@"%@",self.eventList);
}

- (void)setEventList:(NSArray *)eventList
{
    _eventList = eventList;
    [self.tableView reloadData];
}

#pragma mark - build up table
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"%lu",(unsigned long)[_eventList count]);
    return [_eventList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"aloha"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"aloha"];
    }
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    cell.imageView.image = [UIImage imageNamed:_eventList[indexPath.row][@"imgURL"]];
    cell.textLabel.text = _eventList[indexPath.row][@"title"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@",_eventList[indexPath.row][@"startDate"],_eventList[indexPath.row][@"endDate"]];
    return cell;
}
#pragma mark- Segue related
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"detailSegue" sender:indexPath];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath* indexPath = (NSIndexPath*)sender;
    detailViewController* vc = (detailViewController*)[segue destinationViewController];
    //NSDictionary* eventDetial = _eventList[indexPath.row];
    vc.eventDetail = _eventList[indexPath.row];
}

@end
