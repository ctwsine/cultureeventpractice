//
//  AppDelegate.h
//  140223_CultureEventPractice
//
//  Created by ctwsine on 2/23/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
