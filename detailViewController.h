//
//  detailViewController.h
//  140223_CultureEventPractice
//
//  Created by ctwsine on 2/25/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface detailViewController : UIViewController

@property (strong,nonatomic) NSDictionary* eventDetail;

@end
