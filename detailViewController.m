//
//  detailViewController.m
//  140223_CultureEventPractice
//
//  Created by ctwsine on 2/25/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import "detailViewController.h"
#import <SIAlertView.h>
#import "mapViewController.h"
@interface detailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@end

@implementation detailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.titleLabel.text =  _eventDetail[@"title"];
    self.titleLabel.font = [UIFont fontWithName:@"Trebuchet-BoldItalic" size:15.5];
    self.timeLabel.text = [NSString stringWithFormat:@"%@ - %@",_eventDetail[@"startDate"],_eventDetail[@"endDate"]];
    self.locationLabel.text = _eventDetail[@"showInfo"][0][@"locationName"];
    self.descriptionLabel.text = _eventDetail[@"description"];
    
    }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setEventDetail:(NSDictionary *)eventDetail
{
    _eventDetail = eventDetail;
//    self.titleLabel.text = _eventDetail[@"title"];
//    //NSLog(@"%@",_eventDetail[@"title"]);
//    self.timeLabel.text = [NSString stringWithFormat:@"%@ - %@",_eventDetail[@"startDate"],_eventDetail[@"endDate"]];
//    self.descriptionLabel.text = _eventDetail[@"description"];
    UIButton* theButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    theButton.frame = CGRectMake(25, 400, 270, 30);
    [theButton setTitle:[NSString stringWithFormat:@"前往購票(%@)",_eventDetail[@"sourceWebName"]] forState:UIControlStateNormal];
    [theButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [theButton setBackgroundColor:[UIColor greenColor]];
    theButton.layer.cornerRadius = 10;
    [self.view addSubview:theButton];
    
    [theButton addTarget:self action:@selector(theButtonClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(void)theButtonClicked
{
    [self showAlert];
    //NSLog(@"This feature is currently under construction");
    //[self performSegueWithIdentifier:@"mapViewController" sender:nil];
}

-(void)showAlert
{
    SIAlertView* theAlert = [[SIAlertView alloc]initWithTitle:@"通知" andMessage:@"本功能尚未啟用，敬請期待！"];
    [theAlert addButtonWithTitle:@"確定" type:SIAlertViewButtonTypeCancel handler:nil];
    [theAlert show];
}
- (IBAction)goToMap:(id)sender {
    NSLog(@"goToMap clicked");
    [self performSegueWithIdentifier:@"mapViewSegue" sender:sender];
}
//
//- (void)performSegueWithIdentifier:(NSString *)identifier sender:(id)sender
//{
//    NSLog(@"abc");
//}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    mapViewController*mv = (mapViewController*) [segue destinationViewController];
    mv.testString = @"123";
    
}

@end
